import React from "react";
import Content from "./Layout/Content";
import { BrowserRouter, Switch } from "react-router-dom";

function App() {
  return (
    <div>
      <BrowserRouter>
        <div className="content-warpper">
          <Switch>
            <Content />
          </Switch>
        </div>
      </BrowserRouter>
    </div>
  );
}

export default App;
