import React from "react";
import Table from "react-bootstrap/Table";
import FormInput from "./formInput";
import Detail from "./detail";
import Modal from "react-bootstrap/Modal";
import { Pagination, PaginationItem, PaginationLink } from "reactstrap";
import souvenirStockService from "../Service/souvenirStockService";
import souvenirService from "../Service/souvenirService";

class souvenirStock extends React.Component {
  m_souvenir_stock = {
    type: "additional",
    received_by: "",
    tgl: "",
    note: "",
    souvenir: [{ id: "", qty: "", s_note: "" }]
  };

  constructor() {
    super();
    this.state = {
      open_modal: false,
      open_detail: false,
      m_souvenir_stock: this.m_souvenir_stock,
      list_souvenir_stock: [],
      list_souvenir: [],
      list_employee: [],
      mode: "",
      souvenir: {
        id: "",
        qty: "",
        s_note: ""
      },
      errors: "",
      filter: {
        searchCode: "",
        searchRB: "",
        searchRD: "",
        searchCD: "",
        searchCB: "",
        order: "",
        page: "1",
        pagesize: "10"
      }
    };
  }

  handleAddInput = () => {
    this.getSouvenir();
    const { m_souvenir_stock, souvenir } = this.state;
    m_souvenir_stock.souvenir.push(souvenir);
    console.log(m_souvenir_stock.souvenir);
  };

  handleDelete = i => {
    this.getSouvenir();
    const { m_souvenir_stock } = this.state;
    m_souvenir_stock.souvenir.pop(m_souvenir_stock.souvenir[i]);
  };

  // ubah_souvenir = (e, index) => {
  //   const { name, value } = e.target;
  //   const list = [...this.m_souvenir_stock.souvenir];
  //   list[index][name] = value;
  //   this.state.m_souvenir_stock.souvenir(list);
  // };

  changeHandler_souvenir = name => ({ target: { value } }) => {
    this.setState({
      m_souvenir_stock: {
        ...this.state.m_souvenir_stock.souvenir,
        [name]: value
      }
    });
  };

  selectHandler_souvenir = name => ({ target: { value } }) => {
    this.setState({
      m_souvenir_stock: {
        ...this.state.m_souvenir_stock.souvenir,
        [name]: value
      }
    });
  };

  getSouvenir = async () => {
    const respon = await souvenirService.getDataSouvenir();
    if (respon.success) {
      this.setState({
        list_souvenir: respon.result
      });
    }
  };

  getEmployee = async () => {
    const respon = await souvenirStockService.getDataEmployee();
    if (respon.success) {
      this.setState({
        list_employee: respon.result
      });
    }
  };

  changeHandler = name => ({ target: { value } }) => {
    this.setState({
      m_souvenir_stock: {
        ...this.state.m_souvenir_stock,
        [name]: value
      }
    });
  };

  open_CE = () => {
    this.getEmployee();
    this.setState({
      open_modal: true,
      errors: {},
      m_souvenir_stock: this.state.m_souvenir_stock,
      mode: "create"
    });
  };

  cancel_CE = () => {
    this.setState({
      open_modal: false
    });
  };

  closeDetail = () => {
    this.setState({
      open_detail: false
    });
  };

  handleValidation = () => {
    // const { m_souvenir } = this.state;

    let fields = this.state.m_souvenir_stock;
    let errors = {};
    let formIsValid = true;

    if (!fields["received_by"]) {
      formIsValid = false;
      errors["received_by"] = "Jangan Kosong !";
    }

    if (!fields["tgl"]) {
      formIsValid = false;
      errors["tgl"] = "Jangan Kosong !";
    }

    this.setState({ errors: errors });
    return formIsValid;
  };

  loadList = async filter => {
    const respon = await souvenirStockService.getData(filter);
    if (respon.success) {
      this.setState({
        list_souvenir_stock: respon.result
      });
    }
  };

  onChangePage = number => {
    this.setState({
      filter: {
        ...this.state.filter,
        ["page"]: number
      }
    });
  };

  renderPagination() {
    let items = [];
    const { filter, totaldata } = this.state;
    for (let number = 1; number <= totaldata; number++) {
      items.push(
        <PaginationItem key={number} active={number === filter.page}>
          <PaginationLink onClick={() => this.onChangePage(number)} next>
            {number}
          </PaginationLink>
        </PaginationItem>
      );
    }
    return <Pagination>{items}</Pagination>;
  }

  FilterchangeHandler = name => ({ target: { value } }) => {
    this.setState({
      filter: {
        ...this.state.filter,
        [name]: value
      }
    });
  };

  on_cari = () => {
    const { filter } = this.state;
    this.loadList(filter);
  };

  componentDidMount() {
    const { filter } = this.state;
    this.loadList(filter);
  }

  selectHandler_employee = name => ({ target: { value } }) => {
    this.setState({
      m_souvenir_stock: {
        ...this.state.m_souvenir_stock,
        [name]: value
      }
    });
  };

  hendlerEdit = async id => {
    this.getEmployee();
    const respon = await souvenirStockService.getdatabyid(id);
    if (respon.success) {
      this.setState({
        open_modal: true,
        mode: "edit",
        m_souvenir_stock: respon.result[0]
      });
    } else {
      alert(respon.result);
    }
    this.setState({
      errors: {}
    });
  };

  onSave = async () => {
    const { m_souvenir_stock, mode, filter } = this.state;

    if (mode === "create") {
      if (this.handleValidation()) {
        const respon = await souvenirStockService.post(m_souvenir_stock);
        if (respon.success) {
          alert(respon.result);
        } else {
          alert(respon.result);
        }
        this.loadList(filter);
        this.setState({
          open_modal: false
        });
      }
    } else {
      if (this.handleValidation()) {
        const respon = await souvenirStockService.updateData(m_souvenir_stock);
        if (respon.success) {
          alert(respon.result);
        } else {
          alert(respon.result);
        }
        this.loadList(filter);
        this.setState({
          open_modal: false
        });
      }
    }
  };

  render() {
    const {
      open_modal,
      errors,
      m_souvenir_stock,
      mode,
      list_souvenir,
      list_souvenir_stock,
      list_employee,
      souvenir
    } = this.state;
    return (
      <div>
        &nbsp;<h3>&nbsp;List Souvenir Stock</h3>
        <FormInput
          open_modal={open_modal}
          cancel_CE={this.cancel_CE}
          mode={mode}
          changeHandler={this.changeHandler}
          errors={errors}
          m_souvenir_stock={m_souvenir_stock}
          onSave={this.onSave}
          list_souvenir={list_souvenir}
          selectHandler_unit={this.selectHandler_unit}
          list_employee={list_employee}
          selectHandler_employee={this.selectHandler_employee}
          handleAddInput={this.handleAddInput}
          souvenir={souvenir}
          list_souvenir={list_souvenir}
          selectHandler_souvenir={this.selectHandler_souvenir}
          changeHandler_souvenir={this.changeHandler_souvenir}
          handleDelete={this.handleDelete}
        />
        <div class="float-right">
          <button type="button" class="btn btn-primary" onClick={this.open_CE}>
            Add
          </button>
        </div>
        <br />
        <br />
        <div class="float-right">
          <button type="button" class="btn btn-warning" onClick={this.on_cari}>
            Search
          </button>
        </div>
        <div class="form-row">
          <div class="col-md-3 mb-3">
            <input
              type="text"
              placeholder="Transaction Code"
              id="cariCode"
              class="form-control"
              onChange={this.FilterchangeHandler("searchCode")}
            />
          </div>
          <div class="col-md-2 mb-3">
            <input
              type="text"
              placeholder="Received By"
              id="cariName"
              class="form-control"
              onChange={this.FilterchangeHandler("searchRB")}
            />
          </div>
          <div class="col-md-3 mb-1">
            <input
              type="date"
              id="cariDT"
              class="form-control"
              onChange={this.FilterchangeHandler("searchRD")}
            />
          </div>
          <div class="col-md-2 mb-1">
            <input
              type="date"
              class="form-control"
              onChange={this.FilterchangeHandler("searchCD")}
            />
          </div>
          <div class="col-md-2 mb-3">
            <input
              type="text"
              placeholder="Search Created"
              id="cariCB"
              class="form-control"
              onChange={this.FilterchangeHandler("searchCB")}
            />
          </div>
        </div>
        <Table striped bordered hover>
          <thead>
            <tr>
              <th>Souvenir Code</th>
              <th>Received By</th>
              <th>Received Date</th>
              <th>Created By</th>
              <th>Created Date</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody>
            {list_souvenir_stock.map(data => {
              return (
                <tr>
                  <td>{data.code}</td>
                  <td>
                    {data.first_name} {data.last_name}
                  </td>
                  <td>{data.tgl}</td>
                  <td>{data.created_by}</td>
                  <td>{data.tglcd}</td>
                  <td>
                    <div class="btn-group">
                      <button
                        class="btn btn-success"
                        onClick={() => this.openDetail(data.id)}
                      >
                        <i class="fa fa-search"></i>
                      </button>
                      <button
                        class="btn btn-primary"
                        onClick={() => this.hendlerEdit(data.id)}
                      >
                        <i class="fa fa-pen"></i>
                      </button>
                    </div>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </Table>
        <div class="float-right">{this.renderPagination()}</div>
      </div>
    );
  }
}

export default souvenirStock;
