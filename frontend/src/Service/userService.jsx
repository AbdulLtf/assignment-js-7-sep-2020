import axios from "axios";
import { config } from "../Config/config";
export const userService = {
  getDataUsers: filter => {
    const result = axios
      .post(config.apiUrl + "/get_users", filter)

      .then(respons => {
        return {
          success: respons.data.success,
          result: respons.data.result
        };
      })
      .catch(error => {
        return {
          success: false,
          result: error
        };
      });
    //console.log(result);
    return result;
  },
  post: item => {
    const result = axios
      .post(config.apiUrl + "/user_post", item)
      .then(respons => {
        return {
          success: respons.data.success,
          result: respons.data.result
        };
      })
      .catch(error => {
        return {
          success: false,
          result: error
        };
      });

    return result;
  }
};
export default userService;
