import axios from "axios";
import { config } from "../Config/config";
export const tEventService = {
  getDataEvent: filter => {
    const result = axios
      .post(config.apiUrl + "/get_transaksi_event", filter)

      .then(respons => {
        return {
          success: respons.data.success,
          result: respons.data.result
        };
      })
      .catch(error => {
        return {
          success: false,
          result: error
        };
      });
    //console.log(result);
    return result;
  }
};
export default tEventService;
