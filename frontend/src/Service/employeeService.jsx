import axios from "axios";
import { config } from "../Config/config";

export const employeeService = {
  // get by post untuk searching
  getData: filter => {
    const result = axios
      .post(config.apiUrl + "/get_employee", filter)

      .then(respons => {
        return {
          success: respons.data.success,
          result: respons.data.result
        };
      })
      .catch(error => {
        return {
          success: false,
          result: error
        };
      });
    //console.log(result);
    return result;
  },

  // get by get hanya untuk data
  getDataEmployee: () => {
    const result = axios
      .get(config.apiUrl + "/get_employees")

      .then(respons => {
        return {
          success: respons.data.success,
          result: respons.data.result
        };
      })
      .catch(error => {
        return {
          success: false,
          result: error
        };
      });
    //console.log(result);
    return result;
  },

  getdatabyid: id => {
    const result = axios
      .get(config.apiUrl + "/get_employeeNumber/" + id)
      .then(respons => {
        return {
          success: respons.data.success,
          result: respons.data.result
        };
      })
      .catch(error => {
        return {
          success: false,
          result: error
        };
      });

    return result;
  },

  deleteEmployee: item => {
    const result = axios
      .put(config.apiUrl + "/deleteEmployee/" + item.id)
      .then(respons => {
        return {
          success: respons.data.success,
          result: respons.data.result
        };
      })
      .catch(error => {
        return {
          success: false,
          result: error
        };
      });

    return result;
  },

  post: item => {
    const result = axios
      .post(config.apiUrl + "/employee_post", item)
      .then(respons => {
        return {
          success: respons.data.success,
          result: respons.data.result
        };
      })
      .catch(error => {
        return {
          success: false,
          result: error
        };
      });

    return result;
  },

  updateData: item => {
    const result = axios
      .put(config.apiUrl + "/updateEmployee/" + item.id, item)
      .then(respons => {
        return {
          success: respons.data.success,
          result: respons.data.result
        };
      })
      .catch(error => {
        return {
          success: false,
          result: error
        };
      });

    return result;
  },

  validasi: item => {
    const result = axios
      .post(config.apiUrl + "/cekData", item)
      .then(respons => {
        return {
          success: respons.data.success,
          result: respons.data.result
        };
      })
      .catch(error => {
        return {
          success: false,
          result: error
        };
      });

    return result;
  },

  validasiUbah: item => {
    const result = axios
      .post(config.apiUrl + "/cekData_ubah", item)
      .then(respons => {
        return {
          success: respons.data.success,
          result: respons.data.result
        };
      })
      .catch(error => {
        return {
          success: false,
          result: error
        };
      });

    return result;
  }
};

export default employeeService;
