import axios from "axios";
import { config } from "../Config/config";

export const roleService = {
  getDataRole: () => {
    const result = axios
      .get(config.apiUrl + "/get_role")

      .then(respons => {
        return {
          success: respons.data.success,
          result: respons.data.result
        };
      })
      .catch(error => {
        return {
          success: false,
          result: error
        };
      });
    //console.log(result);
    return result;
  }
};
export default roleService;
