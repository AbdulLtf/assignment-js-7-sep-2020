import React from "react";
import Modal from "react-bootstrap/Modal";

class reject extends React.Component {
  render() {
    const {
      close_reject,
      m_event,
      reject,
      open_reject,
      changeHandler,
    } = this.props;
    return (
      <Modal show={open_reject}>
        <Modal.Header style={{ backgroundColor: "lightblue" }}>
          <Modal.Title>
            <label>Reject Reason</label>
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {JSON.stringify(m_event)}
          <form>
            <div class="form-group">
              <label>Note</label>
              <textarea type="text" class="form-control" onChange={changeHandler("rejectreason")}></textarea>
            </div>
          </form>
        </Modal.Body>
        <Modal.Footer>
          <div className="btn-group">
            <button class="btn btn-danger" onClick={reject}>
              Reject
            </button>
            <button class="btn btn-warning" onClick={close_reject}>
              Cancel
            </button>
          </div>
        </Modal.Footer>
      </Modal>
    );
  }
}

export default reject;
