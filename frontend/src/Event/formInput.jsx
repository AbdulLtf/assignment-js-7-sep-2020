import React from "react";
import Modal from "react-bootstrap/Modal";

class formInput extends React.Component {
  render() {
    const {
      open_modal,
      cancel_CE,
      mode,
      changeHandler,
      errors,
      onSave,
      list_event,
      m_event
    } = this.props;
    return (
      <Modal show={open_modal}>
        <Modal.Header style={{ backgroundColor: "lightblue" }}>
          <Modal.Title>
            {mode === "create" ? (
              <label>Add Event Request</label>
            ) : (
              <label>Edit Event Request - {m_event.code}</label>
            )}
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {JSON.stringify(m_event)}
          <form>
            <div class="row">
              <div class="col">
                <label>Event Name *</label>
                <input
                  type="text"
                  class="form-control"
                  placeholder="Type Event Name"
                  onChange={changeHandler("eventname")}
                  value={m_event.eventname}
                />
                <span style={{ color: "red" }}>{errors["eventname"]}</span>
              </div>

              <div class="col">
                <label>Event Place *</label>
                <input
                  type="text"
                  class="form-control"
                  placeholder="Type Event Place"
                  onChange={changeHandler("place")}
                  value={m_event.place}
                />
                <span style={{ color: "red" }}>{errors["place"]}</span>
              </div>
            </div>
            <br />
            <div class="row">
              <div class="col">
                <label>Event Start Date *</label>
                <input
                  type="date"
                  class="form-control"
                  onChange={changeHandler("startdate")}
                  value={m_event.startdate}
                />
                <span style={{ color: "red" }}>{errors["startdate"]}</span>
              </div>

              <div class="col">
                <label>Event End Date *</label>
                <input
                  type="date"
                  class="form-control"
                  onChange={changeHandler("enddate")}
                  value={m_event.enddate}
                />
                <span style={{ color: "red" }}>{errors["enddate"]}</span>
              </div>
            </div>
            <br />
            <div class="row">
              <div class="col">
                <label>Event Budget *</label>
                <input
                  type="text"
                  class="form-control"
                  placeholder="Type Budget"
                  onChange={changeHandler("budget")}
                  value={m_event.budget}
                />
                <span style={{ color: "red" }}>{errors["budget"]}</span>
              </div>

              <div class="col">
                <label>Status</label>
                <input
                  type="text"
                  class="form-control"
                  value={
                    m_event.status === 1
                      ? "Submited"
                      : m_event.status === 2
                      ? "In Progress"
                      : "Done"
                  }
                  disabled
                />
              </div>
            </div>
            <br />
            <div class="form-group">
              <label>Note</label>
              <textarea
                type="text"
                class="form-control"
                placeholder="Type Note"
                onChange={changeHandler("note")}
              >
                {m_event.note === "null" ? "" : m_event.note}
              </textarea>
            </div>
          </form>
        </Modal.Body>
        <Modal.Footer>
          <div className="btn-group">
            <button class="btn btn-primary" onClick={onSave}>
              Save
            </button>
            <button class="btn btn-danger" onClick={cancel_CE}>
              Cancel
            </button>
          </div>
        </Modal.Footer>
      </Modal>
    );
  }
}

export default formInput;
