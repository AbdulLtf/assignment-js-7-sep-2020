import React from "react";
import Modal from "react-bootstrap/Modal";

class detail extends React.Component {
  render() {
    const {
      closeDetail,
      Done,
      m_event,
      open_detail,
      list_employee,
    } = this.props;
    return (
      <Modal show={open_detail}>
        <Modal.Header style={{ backgroundColor: "lightblue" }}>
          <Modal.Title>
            <label>Approved Event Request - {m_event.code}</label>
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {/* {JSON.stringify(m_event)} */}
          <form>
            <div class="row">
              <div class="col">
                <label>Event Name *</label>
                <input
                  type="text"
                  class="form-control"
                  value={m_event.eventname}
                  disabled
                />
              </div>

              <div class="col">
                <label>Event Place *</label>
                <input
                  type="text"
                  class="form-control"
                  value={m_event.place}
                  disabled
                />
              </div>
            </div>
            <br />
            <div class="row">
              <div class="col">
                <label>Event Start Date *</label>
                <input
                  type="date"
                  class="form-control"
                  value={m_event.startdate}
                  disabled
                />
              </div>

              <div class="col">
                <label>Event End Date *</label>
                <input
                  type="date"
                  class="form-control"
                  value={m_event.enddate}
                  disabled
                />
              </div>
            </div>
            <br />
            <div class="row">
              <div class="col">
                <label>Event Budget *</label>
                <input
                  type="text"
                  class="form-control"
                  value={m_event.budget}
                  disabled
                />
              </div>

              <div class="col">
                <label>Status</label>
                <input
                  type="text"
                  class="form-control"
                  value={
                    m_event.status === 1
                      ? "Submited"
                      : m_event.status === 2
                      ? "In Progress"
                      : "Done"
                  }
                  disabled
                />
              </div>
            </div>
            <br />
            <div class="form-group">
              <label>Note</label>
              <textarea type="text" class="form-control" disabled></textarea>
            </div>

            <div class="from-group">
              <label>Assign To *</label>
              <select class="form-control" disabled>
                {list_employee.map((data) => {
                  return (
                    <option
                      value={data.id}
                      selected={m_event.assignto === data.id}
                    >
                      {data.first_name} {data.last_name}
                    </option>
                  );
                })}
              </select>
            </div>
          </form>
        </Modal.Body>
        <Modal.Footer>
          <div className="btn-group">
            {m_event.status === 1 ? (
              <button class="btn btn-primary" disabled>
              Close Request
            </button>
            ) : (
              <button class="btn btn-primary" onClick={Done}>
                Close Request
              </button>
            )}
            <button class="btn btn-warning" onClick={closeDetail}>
              Cancel
            </button>
          </div>
        </Modal.Footer>
      </Modal>
    );
  }
}

export default detail;
