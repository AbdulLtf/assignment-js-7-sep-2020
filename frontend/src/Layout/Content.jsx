import React from "react";
import Sidebar from "../Layout/Sidebar";
import Header from "../Layout/Header";
import { Route, Switch } from "react-router-dom";
import Home from "./Home";
import Souvenir from "../Master/Souvenir";
import Unit from "../Master/Unit";
import Employee from "../Master/Employee";
import User from "../Master/User";
import souvenirStock from "../SouvenirStock";
import Event from "../Event";

function App() {
  return (
    <div className="wrapper">
      <Header />
      <Sidebar />
      <div className="content-wrapper">
        <section className="content">
          <Switch>
            <Route exact path="/" component={Home} />
            <Route exact path="/souvenir" component={Souvenir} />
            <Route exact path="/unit" component={Unit} />
            <Route exact path="/employee" component={Employee} />
            <Route exact path="/user" component={User} />
            <Route exact path="/souvenirstock" component={souvenirStock} />
            <Route exact path="/event" component={Event} />
          </Switch>
        </section>
      </div>
    </div>
  );
}

export default App;
