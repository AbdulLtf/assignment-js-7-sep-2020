import React from "react";
import "./header.css";
export default class Header extends React.Component {
  render() {
    return (
      <nav class="main-header navbar navbar-expand navbar-white navbar-light">
        <ul class="navbar-nav">
          <li class="nav-item">
            <div className="row">
              <div className="col-1">
                <a class="nav-link" data-widget="pushmenu" href="#">
                  <i class="fas fa-bars"></i>
                </a>
              </div>
              <div className="col-10">
                <div className="header-markom">
                  <div className="header-title">MARKOM PROJECT</div>
                </div>
              </div>
            </div>
          </li>
        </ul>
      </nav>
    );
  }
}
