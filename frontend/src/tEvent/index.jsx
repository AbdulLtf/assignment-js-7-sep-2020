import React from "react";
import Table from "react-bootstrap/Table";
// import Modal from "react-bootstrap/Modal";
import tEventService from "../Service/tEventService";
class tEvent extends React.Component {
  m_tEvent = {
    code: "",
    eventName: "",
    startDate: "",
    endDate: "",
    place: "",
    budget: "",
    requestBy: "",
    requestDate: "",
    note: ""
  };
  constructor() {
    super();
    this.state = {
      open_modal: false,
      open_detail: false,
      open_delete: false,
      list_tEvent: [],
      mode: "",
      m_tEvent: this.m_tEvent,
      errors: {},
      filter: {
        searchCode: "",
        searchRD: "",
        searchRB: "",
        searchST: "",
        searchCD: "",
        searchCB: "",
        order: "",
        page: "1",
        pagesize: "10"
      }
    };
  }
  componentDidMount() {
    this.loadList(this.state.filter);
  }

  loadList = async filter => {
    const respon = await tEventService.getDataEvent(filter);
    // alert(JSON.stringify(respon));
    if (respon.success) {
      this.setState({
        list_tEvent: respon.result
      });
    }
  };
  FilterchangeHandler = name => ({ target: { value } }) => {
    this.setState({
      filter: {
        ...this.state.filter,
        [name]: value
      }
    });
  };
  on_cari = () => {
    this.loadList(this.state.filter);
    // alert(JSON.stringify(this.state.filter));
  };
  render() {
    const { list_tEvent } = this.state;
    return (
      <div>
        <h3>List Transaksi Event</h3>
        <div class="float-right">
          <button type="button" class="btn btn-primary" onClick={this.open}>
            Add
          </button>
        </div>
        <div class="float-right">
          <button type="button" class="btn btn-warning" onClick={this.on_cari}>
            Search
          </button>
        </div>
        <div class="form-row">
          <div class="col-md-2 mb-3">
            <input
              type="text"
              placeholder="Event Code"
              id="cariCode"
              class="form-control"
              onChange={this.FilterchangeHandler("searchCode")}
            />
          </div>
          <div class="col-md-2 mb-3">
            <input
              type="text"
              placeholder="Request By"
              id="cariRB"
              class="form-control"
              onChange={this.FilterchangeHandler("searchRB")}
            />
          </div>
          <div class="col-md-2 mb-3">
            <input
              type="date"
              placeholder="Request Date"
              id="cariRD"
              class="form-control"
              onChange={this.FilterchangeHandler("searchRD")}
            />
          </div>
          <div class="col-md-2 mb-3">
            <input
              type="text"
              placeholder="Status"
              id="cariST"
              class="form-control"
              onChange={this.FilterchangeHandler("searchST")}
            />
          </div>
          <div class="col-md-2 mb-3">
            <input
              type="date"
              id="cariCD"
              class="form-control"
              onChange={this.FilterchangeHandler("searchCD")}
            />
          </div>
          <div class="col-md-2 mb-3">
            <input
              type="text"
              placeholder="Created By"
              id="cariCB"
              class="form-control"
              onChange={this.FilterchangeHandler("searchCB")}
            />
          </div>
        </div>
        <Table striped bordered hover>
          <thead>
            <tr>
              <th>No</th>
              <th>Transaction Code</th>
              <th>Request By</th>
              <th>Request Date</th>
              <th>Status</th>
              <th>Created Date</th>
              <th>Created By</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody>
            {list_tEvent.map(data => {
              return (
                <tr>
                  <td>1</td>
                  <td>{data.code}</td>
                  <td>{data.reqby}</td>
                  <td>{data.requestdate}</td>
                  <td>
                    {data.status == 0
                      ? "Rejected"
                      : data.status == 1
                      ? "submitted"
                      : data.status == 2
                      ? "in progress"
                      : "done"}
                  </td>
                  <td>{data.createddate}</td>
                  <td>{data.created_by}</td>
                  <td>
                    <div class="btn-group">
                      <button
                        class="btn btn-success"
                        onClick={() => this.openDetail(data.id)}
                      >
                        <i class="fa fa-search"></i>
                      </button>
                      <button
                        class="btn btn-primary"
                        onClick={() => this.hendlerEdit(data.id)}
                      >
                        <i class="fa fa-pen"></i>
                      </button>
                      <button
                        class="btn btn-danger"
                        onClick={() => this.hendlerDel(data.id)}
                      >
                        <i class="fa fa-trash"></i>
                      </button>
                    </div>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </Table>
      </div>
    );
  }
}
export default tEvent;
