import React from "react";
import Modal from "react-bootstrap/Modal";

class formInput extends React.Component {
  render() {
    const {
      open_modal,
      cancel_CE,
      mode,
      changeHandler,
      errors,
      onSave,
      list_unit,
      selectHandler_unit,
      m_souvenir,
    } = this.props;
    return (
      <Modal show={open_modal}>
        <Modal.Header style={{ backgroundColor: "lightblue" }}>
          <Modal.Title>
            {mode === "create" ? (
              <label>Add Souvenir</label>
            ) : (
              <label>Update Souvenir - {m_souvenir.name} ({m_souvenir.code})</label> 
            )}
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <form>
            <div class="form-group">
              <label>Souvenir Name *</label>
              <input
                type="text"
                class="form-control"
                placeholder="Type Souvenir Name"
                onChange={changeHandler("name")}
                value={m_souvenir.name}
              />
              <span style={{ color: "red" }}>{errors["name"]}</span>
            </div>
            <div class="form-group">
              <label>Unit Name *</label>
              <select
                class="form-control"
                onChange={selectHandler_unit("mUnitId")}
              >
                <option disabled selected>
                  -Choose Unit-
                </option>
                {list_unit.map((data) => {
                  return (
                    <option
                      value={data.id}
                      selected={m_souvenir.unitId === data.id}
                    >
                      {data.name}
                    </option>
                  );
                })}
              </select>
              <span style={{ color: "red" }}>{errors["mUnitId"]}</span>
            </div>
            <div class="form-group">
              <label>Description</label>
              <input
                type="text"
                class="form-control"
                placeholder="Type Description"
                onChange={changeHandler("description")}
                value={
                  m_souvenir.description === "null" ? "" : m_souvenir.description
                }
              />
            </div>
          </form>
        </Modal.Body>
        <Modal.Footer>
          <div className="btn-group">
            <button class="btn btn-primary" onClick={onSave}>
              Save
            </button>
            <button class="btn btn-danger" onClick={cancel_CE}>
              Cancel
            </button>
          </div>
        </Modal.Footer>
      </Modal>
    );
  }
}

export default formInput;
