import React from "react";
import Table from "react-bootstrap/Table";
import FormInput from "./formInput";
import Detail from "./detail";
import Modal from "react-bootstrap/Modal";
import unitService from "../../Service/unitService";
import { Pagination, PaginationItem, PaginationLink } from "reactstrap";

class Unit extends React.Component {
  m_unit = {
    name: "",
    description: ""
  };

  constructor() {
    super();
    this.state = {
      open_modal: false,
      open_delete: false,
      open_detail: false,
      m_unit: this.m_unit,
      list_unit: [],
      data_unit: [],
      mode: "",
      errors: {},
      filter: {
        searchCode: "",
        searchName: "",
        searchCB: "",
        searchDT: "",
        order: "",
        page: "1",
        pagesize: "5"
      },
      totaldata: 1
    };
  }

  changeHandler = name => ({ target: { value } }) => {
    this.setState({
      m_unit: {
        ...this.state.m_unit,
        [name]: value
      }
    });
  };

  open = () => {
    this.setState({
      open_modal: true,
      mode: "create",
      m_unit: { name: "", description: "" },
      errors: {}
    });
  };

  close = () => {
    this.setState({
      open_modal: false
    });
  };

  openDetail = async code => {
    const respon = await unitService.getdatabycode(code);
    if (respon.success) {
      this.setState({
        open_detail: true,
        m_unit: respon.result[0]
      });
    } else {
      alert(respon.result);
    }
  };

  closeDetail = () => {
    this.setState({
      open_detail: false
    });
  };

  closeDelete = () => {
    this.setState({
      open_delete: false
    });
  };

  handleValidation = () => {
    // const { m_unit } = this.state;

    let fields = this.state.m_unit;
    let errors = {};
    let formIsValid = true;

    if (!fields["name"]) {
      formIsValid = false;
      errors["name"] = "Jangan Kosong !";
    }

    this.setState({ errors: errors });
    return formIsValid;
  };

  loadList = async filter => {
    const respon = await unitService.getAll(filter);
    if (respon.success) {
      this.setState({
        list_unit: respon.result
      });
    }
  };

  dataUnit = async () => {
    const respon = await unitService.getDataUnit();
    if (respon.success) {
      this.setState({
        data_unit: respon.result
      });
    }
  };

  componentDidMount() {
    const { filter } = this.state;
    this.loadList(filter);
    this.dataUnit();
  }

  hendlerEdit = async code => {
    const respon = await unitService.getdatabycode(code);
    if (respon.success) {
      this.setState({
        open_modal: true,
        mode: "edit",
        m_unit: respon.result[0]
      });
    } else {
      alert(respon.result);
    }
    this.setState({
      errors: {}
    });
  };

  onSave = async () => {
    const { m_unit, mode, filter } = this.state;

    if (mode === "create") {
      if (this.handleValidation()) {
        const respon = await unitService.post(m_unit);
        if (respon.success) {
          alert(respon.result);
        } else {
          alert(respon.result);
        }
        this.loadList(filter);
        this.setState({
          open_modal: false
        });
      }
    } else {
      if (this.handleValidation()) {
        const respon = await unitService.updateData(m_unit);
        if (respon.success) {
          alert(respon.result);
        } else {
          alert(respon.result);
        }
        this.loadList(filter);
        this.setState({
          open_modal: false
        });
      }
    }
  };

  hendlerDel = async code => {
    const respon = await unitService.getdatabycode(code);
    if (respon.success) {
      this.setState({
        open_delete: true,
        m_unit: respon.result[0]
      });
    } else {
      alert(respon.result);
    }
  };

  sureDelete = async item => {
    const { m_unit, filter } = this.state;

    const respon = await unitService.deleteUnit(m_unit);
    if (respon.success) {
      alert(respon.result);
    } else {
      alert(respon.result);
    }
    this.loadList(filter);
    this.setState({
      open_delete: false
    });
  };

  onChangePage = number => {
    this.setState({
      filter: {
        ...this.state.filter,
        ["page"]: number
      }
    });
  };

  renderPagination() {
    let items = [];
    const { filter, totaldata } = this.state;
    for (let number = 1; number <= totaldata; number++) {
      items.push(
        <PaginationItem key={number} active={number === filter.page}>
          <PaginationLink onClick={() => this.onChangePage(number)} next>
            {number}
          </PaginationLink>
        </PaginationItem>
      );
    }
    return <Pagination>{items}</Pagination>;
  }

  FilterchangeHandler = name => ({ target: { value } }) => {
    this.setState({
      filter: {
        ...this.state.filter,
        [name]: value
      }
    });
  };

  on_cari = () => {
    const { filter } = this.state;
    this.loadList(filter);
  };

  render() {
    const {
      data_unit,
      open_delete,
      m_unit,
      list_unit,
      open_modal,
      mode,
      errors,
      open_detail
    } = this.state;
    return (
      <div>
        &nbsp; <h3>&nbsp;List Unit</h3>
        <FormInput
          open_modal={open_modal}
          close={this.close}
          m_unit={m_unit}
          onSave={this.onSave}
          mode={mode}
          errors={errors}
          changeHandler={this.changeHandler}
        />
        <Detail
          open_detail={open_detail}
          m_unit={m_unit}
          closeDetail={this.closeDetail}
        />
        <div class="float-right">
          <button type="button" class="btn btn-primary" onClick={this.open}>
            Add
          </button>
        </div>
        <br />
        <br />
        <div class="float-right">
          <button type="button" class="btn btn-warning" onClick={this.on_cari}>
            Search
          </button>
        </div>
        {/* {JSON.stringify(this.state.filter)} */}
        <div class="form-row">
          <div class="col-md-3 mb-3">
            <input
              type="text"
              placeholder="Search Unit Code"
              id="cariCode"
              class="form-control"
              onChange={this.FilterchangeHandler("searchCode")}
            />
          </div>
          <div class="col-md-3 mb-3">
            <select
              class="form-control"
              id="cariName"
              onChange={this.FilterchangeHandler("searchName")}
            >
              <option disabled selected>
                - Search Name -
              </option>
              {data_unit.map(data => {
                return <option value={data.name}>{data.name}</option>;
              })}
            </select>
          </div>
          <div class="col-md-3 mb-3">
            <input
              type="date"
              id="cariDT"
              class="form-control"
              onChange={this.FilterchangeHandler("searchDT")}
            />
          </div>
          <div class="col-md-3 mb-3">
            <input
              type="text"
              placeholder="Search Created"
              id="cariCB"
              class="form-control"
              onChange={this.FilterchangeHandler("searchCB")}
            />
          </div>
        </div>
        <Modal show={open_delete} style={{ opacity: 1 }}>
          <Modal.Body>
            Apakah anda akan menghapus data ini {m_unit.code} ?
          </Modal.Body>
          <Modal.Footer>
            <div class="btn-group">
              <button
                class="btn btn-success"
                onClick={() => this.sureDelete(m_unit.code)}
              >
                Ya
              </button>
              <button class="btn btn-info" onClick={this.closeDelete}>
                Tidak
              </button>
            </div>
          </Modal.Footer>
        </Modal>
        <Table striped bordered hover>
          <thead>
            <tr>
              <th>Unit Code</th>
              <th>Unit Name</th>
              <th>Created By</th>
              <th>Created Date</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody>
            {list_unit.map(data => {
              return (
                <tr>
                  <td>{data.code}</td>
                  <td>{data.name}</td>
                  <td>{data.created_by}</td>
                  <td>{data.tgl}</td>
                  <td>
                    <div class="btn-group">
                      <button
                        class="btn btn-success"
                        onClick={() => this.openDetail(data.code)}
                      >
                        <i class="fa fa-search"></i>
                      </button>
                      <button
                        class="btn btn-primary"
                        onClick={() => this.hendlerEdit(data.code)}
                      >
                        <i class="fa fa-pen"></i>
                      </button>
                      <button
                        class="btn btn-danger"
                        onClick={() => this.hendlerDel(data.code)}
                      >
                        <i class="fa fa-trash"></i>
                      </button>
                    </div>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </Table>
        <div class="float-right">{this.renderPagination()}</div>
      </div>
    );
  }
}

export default Unit;
