import React from "react";
import Modal from "react-bootstrap/Modal";

class Detail extends React.Component {
  render() {
    const { open_detail, m_unit, closeDetail } = this.props;
    return (
      <Modal show={open_detail} style={{ opacity: 1 }}>
        <Modal.Header style={{ background: "lightgrey" }}>
          <Modal.Title>
            View Unit - {m_unit.name} ({m_unit.code})
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {/* {JSON.stringify(m_unit)} */}
          <from>
            <div class="form-group">
              <label>Unit Code *</label>
              <input
                type="text"
                class="form-control"
                value={m_unit.code}
                disabled
              />
            </div>
            <div class="form-group">
              <label>Unit Name *</label>
              <input
                type="text"
                class="form-control"
                value={m_unit.name}
                disabled
              />
            </div>
            <div class="form-group">
              <label>Description</label>
              <input
                type="text"
                class="form-control"
                value={m_unit.description === "null" ? "" : m_unit.description}
                disabled
              />
            </div>
          </from>
        </Modal.Body>
        <Modal.Footer>
          <div className="btn-group">
            <button className="btn btn-danger" onClick={closeDetail}>
              Close
            </button>
          </div>
        </Modal.Footer>
      </Modal>
    );
  }
}

export default Detail;
