import React from "react";
import Modal from "react-bootstrap/Modal";

class formInput extends React.Component {
  render() {
    const {
      open_modal,
      close,
      mode,
      changeHandler,
      errors,
      m_unit,
      onSave
    } = this.props;
    return (
      <Modal show={open_modal} style={{ opacity: 1 }}>
        <Modal.Header style={{ backgroundColor: "lightblue" }}>
          <Modal.Title>
            {mode === "create" ? (
              <label>Add Unit</label>
            ) : (
              <label>
                Update Unit - {m_unit.name} ({m_unit.code}){" "}
              </label>
            )}
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {/* {JSON.stringify(m_unit)} */}
          <from>
            <div class="form-group">
              <label>Unit Name *</label>
              <input
                type="text"
                class="form-control"
                placeholder="Type Unit Name"
                onChange={changeHandler("name")}
                value={m_unit.name}
              />
              <span style={{ color: "red" }}>{errors["name"]}</span>
            </div>
            <div class="form-group">
              <label>Description</label>
              <input
                type="text"
                class="form-control"
                placeholder="Type Description"
                onChange={changeHandler("description")}
                value={m_unit.description}
              />
            </div>
          </from>
        </Modal.Body>
        <Modal.Footer>
          <div class="btn-group">
            <button class="btn btn-primary" onClick={onSave}>
              Save
            </button>
            <button class="btn btn-danger" onClick={close}>
              Cancel
            </button>
          </div>
        </Modal.Footer>
      </Modal>
    );
  }
}
export default formInput;
