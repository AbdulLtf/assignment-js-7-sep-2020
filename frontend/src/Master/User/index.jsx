import React from "react";
import Table from "react-bootstrap/Table";
// import Modal from "react-bootstrap/Modal";
import FormInput from "./formInput";
import userService from "../../Service/userService";
import companyService from "../../Service/companyService";
import roleService from "../../Service/roleService";
import employeeService from "../../Service/employeeService";
class User extends React.Component {
  m_user = {
    username: "",
    password: "",
    password2: "",
    employeeId: "",
    roleId: ""
  };
  constructor() {
    super();
    this.state = {
      open_modal: false,
      open_detail: false,
      open_delete: false,
      list_user: [],
      list_company: [],
      list_role: [],
      list_employee: [],
      mode: "",
      m_user: this.m_user,
      errors: {},
      filter: {
        searchEN: "",
        searchRN: "",
        searchCN: "",
        searchUN: "",
        searchCD: "",
        searchCB: "",
        order: "",
        page: "1",
        pagesize: "10"
      }
    };
  }
  componentDidMount() {
    this.loadList(this.state.filter);
    this.getCompany();
    this.getRole();
    this.getEmployee(this.state.filter);
  }

  loadList = async filter => {
    const respon = await userService.getDataUsers(filter);
    // alert(JSON.stringify(respon));
    if (respon.success) {
      this.setState({
        list_user: respon.result
      });
    }
  };
  getCompany = async () => {
    const respon = await companyService.getDataCompany();
    if (respon.success) {
      this.setState({
        list_company: respon.result
      });
    }
  };
  getRole = async () => {
    const respon = await roleService.getDataRole();
    if (respon.success) {
      this.setState({
        list_role: respon.result
      });
    }
  };
  getEmployee = async () => {
    const respon = await employeeService.getDataEmployee();
    // alert(JSON.stringify(respon));
    if (respon.success) {
      this.setState({
        list_employee: respon.result
      });
    }
  };

  FilterchangeHandler = name => ({ target: { value } }) => {
    this.setState({
      filter: {
        ...this.state.filter,
        [name]: value
      }
    });
  };
  on_cari = () => {
    this.loadList(this.state.filter);
    // alert(JSON.stringify(this.state.filter));
  };

  open = () => {
    this.setState({
      open_modal: true,
      mode: "create",
      m_user: {
        username: "",
        password: "",
        password2: "",
        employeeId: "",
        roleId: ""
      },
      errors: {}
    });
  };

  close = () => {
    this.setState({
      open_modal: false,
      open_detail: false,
      open_delete: false
    });
  };

  selectHandler_employee = name => ({ target: { value } }) => {
    this.setState({
      m_user: {
        ...this.state.m_user,
        [name]: value
      }
    });
  };
  selectHandler_role = name => ({ target: { value } }) => {
    this.setState({
      m_user: {
        ...this.state.m_user,
        [name]: value
      }
    });
  };

  handleValidation = () => {
    // const { m_unit } = this.state;

    let fields = this.state.m_user;
    let errors = {};
    let formIsValid = true;

    if (!fields["username"]) {
      formIsValid = false;
      errors["username"] = "Jangan Kosong !";
    }
    this.state.list_user.map(data => {
      if (!fields["employeeId"]) {
        formIsValid = false;
        errors["employeeId"] = "Jangan Kosong !";
      } else if (fields["employeeId"] == data.m_employee_id) {
        formIsValid = false;
        errors["employeeId"] = "user employee sudah ada !";
      }
    });

    if (!fields["roleId"]) {
      formIsValid = false;
      errors["roleId"] = "Jangan Kosong !";
    }

    if (!fields["password"]) {
      formIsValid = false;
      errors["password"] = "Jangan Kosong !";
    }

    if (!fields["password2"]) {
      formIsValid = false;
      errors["password2"] = "Jangan Kosong !";
    } else if (fields["password2"] != fields["password"]) {
      formIsValid = false;
      errors["password2"] = "password tidak sama !";
    }

    this.setState({ errors: errors });
    return formIsValid;
  };

  changeHandler = name => ({ target: { value } }) => {
    this.setState({
      m_user: {
        ...this.state.m_user,
        [name]: value
      }
    });
  };

  onSave = async () => {
    const { m_user, mode, filter } = this.state;

    if (mode === "create") {
      if (this.handleValidation()) {
        // alert(JSON.stringify(m_user));
        const respon = await userService.post(m_user);
        if (respon.success) {
          alert(respon.result);
        } else {
          alert(respon.result);
        }
        this.loadList(filter);
        this.setState({
          open_modal: false
        });
      }
    } else {
      if (this.handleValidation()) {
        const respon = await userService.updateData(m_user);
        if (respon.success) {
          alert(respon.result);
        } else {
          alert(respon.result);
        }
        this.loadList(filter);
        this.setState({
          open_modal: false
        });
      }
    }
  };

  render() {
    const {
      list_user,
      list_company,
      list_role,
      list_employee,
      mode,
      errors,
      open_modal,
      m_user
    } = this.state;
    return (
      <div>
        <h3>List User</h3>
        <FormInput
          open_modal={open_modal}
          close={this.close}
          mode={mode}
          errors={errors}
          m_user={m_user}
          list_employee={list_employee}
          list_role={list_role}
          changeHandler={this.changeHandler}
          selectHandler_employee={this.selectHandler_employee}
          selectHandler_role={this.selectHandler_role}
          onSave={this.onSave}
        />
        <div class="float-right">
          <button type="button" class="btn btn-primary" onClick={this.open}>
            Add
          </button>
        </div>
        <div class="float-right">
          <button type="button" class="btn btn-warning" onClick={this.on_cari}>
            Search
          </button>
        </div>
        <div class="form-row">
          <div class="col-md-2 mb-3">
            <select
              class="form-control"
              id="cariEN"
              onChange={this.FilterchangeHandler("searchEN")}
            >
              <option selected value="">
                -Select Employee-
              </option>
              {list_user.map(data => {
                return <option value={data.empname}>{data.empname}</option>;
              })}
            </select>
          </div>
          <div class="col-md-2 mb-3">
            <select
              class="form-control"
              id="cariRN"
              onChange={this.FilterchangeHandler("searchRN")}
            >
              <option selected value="">
                -Select Role-
              </option>
              {list_role.map(data => {
                return <option value={data.name}>{data.name}</option>;
              })}
            </select>
          </div>
          <div class="col-md-2 mb-3">
            <select
              class="form-control"
              id="cariCN"
              onChange={this.FilterchangeHandler("searchCN")}
            >
              <option selected value="">
                -Select Company-
              </option>
              {list_company.map(data => {
                return <option value={data.name}>{data.name}</option>;
              })}
            </select>
          </div>
          <div class="col-md-2 mb-3">
            <input
              type="text"
              placeholder="Username"
              id="cariUN"
              class="form-control"
              onChange={this.FilterchangeHandler("searchUN")}
            />
          </div>
          <div class="col-md-2 mb-3">
            <input
              type="date"
              id="cariCD"
              class="form-control"
              onChange={this.FilterchangeHandler("searchCD")}
            />
          </div>
          <div class="col-md-2 mb-3">
            <input
              type="text"
              placeholder="Created By"
              id="cariCB"
              class="form-control"
              onChange={this.FilterchangeHandler("searchCB")}
            />
          </div>
        </div>
        <Table striped bordered hover>
          <thead>
            <tr>
              <th>No</th>
              <th>Employee Name</th>
              <th>Role</th>
              <th>Company</th>
              <th>Username</th>
              <th>Created Date</th>
              <th>Created By</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody>
            {list_user.map((data, x) => {
              return (
                <tr>
                  <td>{x + 1}</td>
                  <td>{data.empname}</td>
                  <td>{data.rlname}</td>
                  <td>{data.compname}</td>
                  <td>{data.username}</td>
                  <td>{data.createddate}</td>
                  <td>{data.created_by}</td>
                  <td>
                    <div class="btn-group">
                      <button
                        class="btn btn-success"
                        onClick={() => this.openDetail(data.id)}
                      >
                        <i class="fa fa-search"></i>
                      </button>
                      <button
                        class="btn btn-primary"
                        onClick={() => this.hendlerEdit(data.id)}
                      >
                        <i class="fa fa-pen"></i>
                      </button>
                      <button
                        class="btn btn-danger"
                        onClick={() => this.hendlerDel(data.id)}
                      >
                        <i class="fa fa-trash"></i>
                      </button>
                    </div>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </Table>
      </div>
    );
  }
}
export default User;
