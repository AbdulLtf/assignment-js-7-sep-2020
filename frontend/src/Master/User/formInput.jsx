import React from "react";
import Modal from "react-bootstrap/Modal";

class formInput extends React.Component {
  render() {
    const {
      open_modal,
      close,
      mode,
      changeHandler,
      errors,
      m_user,
      list_employee,
      list_role,
      onSave,
      selectHandler_employee,
      selectHandler_role
    } = this.props;
    return (
      <Modal size="lg" show={open_modal}>
        <Modal.Header style={{ backgroundColor: "lightblue" }}>
          <Modal.Title>
            {mode === "create" ? (
              <label>Add User</label>
            ) : (
              <label>
                Update User - {m_user.first_name} {m_user.last_name} (
                {m_user.code}){" "}
              </label>
            )}
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {JSON.stringify(m_user)}
          <from>
            <div class="form-group">
              <label>Employee Name *</label>
              <select
                class="form-control"
                onChange={selectHandler_employee("employeeId")}
              >
                <option disabled selected>
                  -Select Employee Name-
                </option>
                {list_employee.map(data => {
                  return (
                    <option
                      value={data.id}
                      selected={m_user.employeeId === data.id}
                    >
                      {data.first_name} {data.last_name}
                    </option>
                  );
                })}
              </select>
              <span style={{ color: "red" }}>{errors["employeeId"]}</span>
            </div>
            <div class="form-group">
              <label>Role Name *</label>
              <select
                class="form-control"
                onChange={selectHandler_role("roleId")}
              >
                <option disabled selected>
                  -Select Role Name-
                </option>
                {list_role.map(data => {
                  return (
                    <option
                      value={data.id}
                      selected={m_user.roleId === data.id}
                    >
                      {data.name}
                    </option>
                  );
                })}
              </select>
              <span style={{ color: "red" }}>{errors["roleId"]}</span>
            </div>
            <div class="form-group">
              <label>Username *</label>
              <input
                type="text"
                class="form-control"
                placeholder="Type Username"
                onChange={changeHandler("username")}
                value={m_user.username}
              />
              <span style={{ color: "red" }}>{errors["username"]}</span>
            </div>
            <div class="form-group">
              <label>Password</label>
              <input
                type="text"
                class="form-control"
                placeholder="Type Password"
                onChange={changeHandler("password")}
                value={m_user.password}
              />
              <span style={{ color: "red" }}>{errors["password"]}</span>
            </div>
            <div class="form-group">
              <label>Re-Password</label>
              <input
                type="text"
                class="form-control"
                placeholder="Type Re-Password"
                onChange={changeHandler("password2")}
                value={m_user.password2}
              />
              <span style={{ color: "red" }}>{errors["password2"]}</span>
            </div>
          </from>
        </Modal.Body>
        <Modal.Footer>
          <div class="btn-group">
            <button class="btn btn-primary" onClick={onSave}>
              Save
            </button>
            <button class="btn btn-danger" onClick={close}>
              Cancel
            </button>
          </div>
        </Modal.Footer>
      </Modal>
    );
  }
}

export default formInput;
