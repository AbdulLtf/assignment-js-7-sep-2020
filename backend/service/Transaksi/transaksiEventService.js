// select*from t_event
// insert into t_event (code,event_name,start_date,end_date,place,budget,request_by,request_date,approved_by,approved_date,assign_to,note,status,is_delete,created_by,created_date)
// values ('TRWOEV1709200001','Pesta','18-09-2020','19-09-2020','Jakarta','20000',1,current_date,2,current_date,2,'ini pesta',1,false,'latief',current_date)
module.exports = exports = (server, pool) => {
  server.post("/api/get_transaksi_event", (req, res) => {
    const {
      searchCode,
      searchRD,
      searchST,
      searchCD,
      searchRB,
      searchCB,
      order,
      page,
      pagesize
    } = req.body;
    let qFilterCode =
      searchCode != "" ? ` AND te."code" LIKE '%${searchCode}%' ` : ``;
    let qFilterRD =
      searchRD != "" ? ` AND te."request_date" LIKE '%${searchRD}%' ` : ``;
    let qFilterST =
      searchST != "" ? ` AND te."status" LIKE '%${searchST}%' ` : ``;
    let qFilterCD =
      searchCD != ""
        ? ` AND te."created_date"::text LIKE '%${searchCD}%' `
        : ``;
    let qFilterRB =
      searchRB != "" ? ` AND te."request_by" LIKE '%${searchRB}%' ` : ``;
    let qFilterCB =
      searchCB != "" ? ` AND te."created_by" LIKE '%${searchCB}%' ` : ``;
    let qOrder =
      order != "" ? `ORDER BY te."code" DESC` : `ORDER BY te."code" ASC`;
    let perpageName = (page - 1) * pagesize;

    var query = `Select te."event_name",te."status", te."code",TO_CHAR(te."request_date" :: DATE, 'yyyy-mm-dd') as requestdate,
            TO_CHAR(te."created_date" :: DATE, 'yyyy-mm-dd') as createddate,concat(e."first_name" || ' ' || e."last_name") as reqby,
            te.created_by
            from "t_event" as te
            join "employee" as e
			on te."request_by" = e."id"
            where te."is_delete" = false
            ${qFilterCode} ${qFilterRD} ${qFilterST} ${qFilterCD} ${qFilterCB} ${qFilterRB} ${qOrder} LIMIT ${pagesize}
            OFFSET ${perpageName}`;
    // var query2 = ``;

    // console.log(query);
    pool.query(query, (error, result) => {
      if (error) {
        res.send(400, {
          success: false,
          result: error
        });
      } else {
        res.send(200, {
          success: true,
          result: result.rows
        });
      }
    });
  });
};
