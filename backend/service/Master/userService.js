module.exports = exports = (server, pool) => {
  server.post("/api/get_users", (req, res) => {
    const {
      searchEN,
      searchRN,
      searchCN,
      searchUN,
      searchCD,
      searchCB,
      order,
      page,
      pagesize
    } = req.body;
    let qFilterEN =
      searchEN != ""
        ? `AND concat(e."first_name" || ' ' || e."last_name") like '%${searchEN}%' `
        : ``;
    let qFilterRN = searchRN != "" ? ` AND r."name" LIKE '%${searchRN}%' ` : ``;
    let qFilterCN = searchCN != "" ? ` AND c."name" LIKE '%${searchCN}%' ` : ``;
    let qFilterUN =
      searchUN != "" ? ` AND us."username" LIKE '%${searchUN}%' ` : ``;
    let qFilterCD =
      searchCD != ""
        ? ` AND us."created_date"::text LIKE '%${searchCD}%' `
        : ``;
    let qFilterCB =
      searchCB != "" ? ` AND us."created_by" LIKE '%${searchCB}%' ` : ``;
    let qOrder = order != "" ? `ORDER BY us."id" DESC` : `ORDER BY us."id" ASC`;
    let perpageName = (page - 1) * pagesize;

    var query = `Select us."username",TO_CHAR(us."created_date" :: DATE, 'dd-mm-yyyy') as createddate,
              concat(e."first_name" || ' ' || e."last_name") as empname,r.name as rlname,
              c.name as compname,us.created_by,us.m_employee_id
              from "users" as us
              join "employee" as e
              on us."m_employee_id" = e."id"
              join "role" as r
              on us."m_role_id" = r."id"
              join "company" as c
              on e."m_company_id" = c."id"
              where us."is_delete" = false ${qFilterEN}
              ${qFilterRN} ${qFilterCN} ${qFilterUN}
              ${qFilterCD} ${qFilterCB} ${qOrder} LIMIT ${pagesize}
              OFFSET ${perpageName}`;

    // console.log(query);
    pool.query(query, (error, result) => {
      if (error) {
        res.send(400, {
          success: false,
          result: error
        });
      } else {
        res.send(200, {
          success: true,
          result: result.rows
        });
      }
    });
  });
  server.post("/api/user_post", (req, res) => {
    console.log(req.body);

    const { username, password, employeeId, roleId } = req.body;

    var query = `INSERT INTO "users"(
               "created_by", "created_date", "username", "password", "m_employee_id", m_role_id , "is_delete")
               VALUES('Administator', current_timestamp, '${username}', '${password}', ${employeeId}, ${roleId},false);`;

    console.log(query);

    pool.query(query, (error, result) => {
      if (error) {
        res.send(400, {
          success: false,
          result: error
        });
      } else {
        res.send(201, {
          success: true,
          result: "Berhasil Disimpan"
        });
      }
    });
  });
};
