module.exports = exports = (server, pool) => {
  server.get("/api/get_company", (req, res) => {
    var query = `select * from company where is_delete = false`;
    pool.query(query, (error, result) => {
      if (error) {
        res.send(400, {
          success: false,
          result: error
        });
      } else {
        res.send(200, {
          success: true,
          result: result.rows
        });
      }
    });
  });
};
